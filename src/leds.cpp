#include <FastLED.h>

#include "state.h"
#include "leds.h"

static led_state_t lstate;
static pattern_state_t pstate;

bool pattern_noop(led_state_t *lstate, pattern_args_t *args) {}

void add_patterns(pattern_func_t *p, int num)
{
  pstate.patterns = p;
  pstate.current = p[0];
  pstate.current_ndx = 0;
  pstate.size = num;
}

void init_led_strip(CRGB *leds, uint8_t size)
{
  lstate.leds = leds;
  lstate.size = size;
  FastLED.addLeds<WS2812, 7, GRB>(leds, size);
}

void update_leds(input_state_t *input)
{
  static int epoch = 0;
  pattern_args_t args;
  args.input = input;
  args.epoch = epoch;

  pstate.current(&lstate, &args);

  epoch++;
}

void next_pattern()
{
  pstate.current_ndx = (pstate.current_ndx + 1) % pstate.size;
  pstate.current = pstate.patterns[pstate.current_ndx];
}

void set_pattern(pattern_func_t func)
{
  pstate.current = func;
}
