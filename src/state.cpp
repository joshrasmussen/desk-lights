#include <Arduino.h>

#include "state.h"

void smart_jitter(uint8_t *knob, uint8_t rawValue)
{
  if (rawValue < (*knob - JITTER) || rawValue > (*knob + JITTER))
  {
    *knob = rawValue;
  }
}

uint8_t analog_byte(int pin)
{
  return map(analogRead(pin), 0, 1023, 0, 255);
}

void update_state(input_state_t *state)
{
  smart_jitter(&state->left_knob, analog_byte(LEFT_KNOB));
  smart_jitter(&state->middle_knob, analog_byte(MIDDLE_KNOB));
  smart_jitter(&state->right_knob, analog_byte(RIGHT_KNOB));

  if (digitalRead(SHIFT_BTN) == HIGH)
  {
    state->shift_btn = false;
  }
  else
  {
    state->shift_btn = true;
  }
}

void init_state(input_state_t *state)
{
  state->left_knob = 0;
  state->middle_knob = 0;
  state->right_knob = 0;
  state->shift_btn = false;

  pinMode(SHIFT_BTN, INPUT_PULLUP);
}
