#ifndef INTERRUPT_H
#define INTERRUPT_H

#define BLUE_BTN 2
#define GREEN_BTN 3

typedef struct interrupt_state
{
  volatile uint8_t blue_btn;
  volatile uint8_t green_btn;
} interrupt_state_t;

void setup_interrupts();

void reset_interrupt_state(int pin);

volatile uint8_t get_interrupt_state(int pin);

#endif