#ifndef STATE_H
#define STATE_H

#define LEFT_KNOB 1
#define MIDDLE_KNOB 2
#define RIGHT_KNOB 3

#define SHIFT_BTN 4

#define JITTER 4

typedef struct input_state
{
  uint8_t left_knob;
  uint8_t middle_knob;
  uint8_t right_knob;
  bool shift_btn;
} input_state_t;

/**
 * Call to update the current values of state
 * with the values at the pins
 */
void update_state(input_state_t *state);

/**
 * Setup the input state to a default state and
 * register any input pins
 */
void init_state(input_state_t *state);

#endif
