#ifndef PATTERNS_H
#define PATTERNS_H

bool manual_color(led_state_t *, pattern_args_t *);

bool uniform_rainbow(led_state_t *, pattern_args_t *);

bool rainbow_wave(led_state_t *, pattern_args_t *);

#endif