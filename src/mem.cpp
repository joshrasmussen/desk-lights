#include <FastLED.h>
#include "mem.h"

void store(CRGB *store, int store_cap, CRGB *leds, int led_cap, int index)
{
  if (index >= store_cap)
  {
    return;
  }

  store[index] = leds[0];
}

void load(CRGB *store, int store_cap, CRGB *leds, int led_cap, int index)
{
  if (index >= store_cap)
  {
    return;
  }

  for (int i = 0; i < led_cap; i++)
  {
    leds[i] = store[index];
  }
}
