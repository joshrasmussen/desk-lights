#include <Arduino.h>
#include <FastLED.h>

#include "state.h"
#include "leds.h"

#include "patterns.h"

bool manual_color(led_state_t *lstate, pattern_args_t *args)
{
  int hue = args->input->left_knob;
  int sat = args->input->middle_knob;
  int value = args->input->right_knob;

  for (int i = 0; i < lstate->size; i++)
  {
    lstate->leds[i] = CHSV(hue, sat, value);
  }

  return true;
}

bool uniform_rainbow(led_state_t *lstate, pattern_args_t *args)
{
  int hue = args->epoch % 255;

  for (int i = 0; i < lstate->size; i++)
  {
    lstate->leds[i] = CHSV(hue, args->input->middle_knob, args->input->right_knob);
  }

  return true;
}

bool rainbow_wave(led_state_t *lstate, pattern_args_t *args)
{
  int hue = args->epoch % 255;

  for (int i = 0; i < lstate->size; i++)
  {
    lstate->leds[i] = CHSV(hue + i, args->input->middle_knob, args->input->right_knob);
  }

  return true;
}