#ifndef MEM_H
#define MEM_H

void store(CRGB *store, int store_cap, CRGB *leds, int led_cap, int index);

void load(CRGB *store, int store_cap, CRGB *leds, int led_cap, int index);

#endif