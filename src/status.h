#ifndef STATUS_H
#define STATUS_H

typedef struct status_leds
{
  CRGB *leds;
  uint8_t size;
  uint8_t selected_ndx;
  uint8_t active_ndx;
} status_leds_t;

void init_status_leds(CRGB *, uint8_t);

void update_status_leds();

uint8_t get_active_ndx();

void deactivate();

void activate(int);

uint8_t get_selected_ndx();

void increment_selected();

#endif
