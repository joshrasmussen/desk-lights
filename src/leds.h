#ifndef LEDS_H
#define LEDS_H

typedef struct led_state
{
  CRGB *leds;
  uint8_t size;
} led_state_t;

typedef struct pattern_args
{
  input_state_t *input;
  int epoch;
} pattern_args_t;

typedef bool (*pattern_func_t)(led_state_t *, pattern_args_t *);

typedef struct pattern_state
{
  pattern_func_t *patterns;
  pattern_func_t current;
  uint8_t current_ndx;
  uint8_t size;
} pattern_state_t;

void update_leds(input_state_t *);

void init_led_strip(CRGB *, uint8_t);

void add_patterns(pattern_func_t *, int);

void next_pattern();

bool pattern_noop(led_state_t *lstate, pattern_args_t *args);

void set_pattern(pattern_func_t func);

#endif
