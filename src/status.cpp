#include <FastLED.h>
#include "status.h"

static status_leds_t sleds;

void init_status_leds(CRGB *leds, uint8_t count)
{
  sleds.leds = leds;
  sleds.size = count;
  sleds.active_ndx = -1;
  sleds.selected_ndx = 0;

  FastLED.addLeds<WS2812, 9, GRB>(leds, count);
}

uint8_t get_active_ndx()
{
  return sleds.active_ndx;
}

void deactivate()
{
  sleds.active_ndx = -1;
}

void activate(int ndx)
{
  sleds.active_ndx = ndx;
}

uint8_t get_selected_ndx()
{
  return sleds.selected_ndx;
}

void increment_selected()
{
  sleds.selected_ndx = (sleds.selected_ndx + 1) % sleds.size;
}

void update_status_leds()
{
  for (int i = 0; i < sleds.size; i++)
  {
    sleds.leds[i] = CRGB(0, 0, 0);
  }

  if (sleds.selected_ndx >= 0 && sleds.selected_ndx < sleds.size)
  {
    sleds.leds[sleds.selected_ndx] = CRGB(0, 0, 255);
  }

  if (sleds.active_ndx >= 0 && sleds.active_ndx < sleds.size)
  {
    sleds.leds[sleds.active_ndx] = CRGB(0, 255, 0);
  }
}