#include <Arduino.h>
#include "interrupt.h"

static volatile interrupt_state_t istate;

void debounce(volatile uint8_t *pin_value, unsigned long last_interrupt_time)
{
  unsigned long interrupt_time = millis();

  if (interrupt_time - last_interrupt_time > 500)
  {
    *pin_value = 1;
  }
  last_interrupt_time = interrupt_time;
}

void blue_btn_isr()
{
  static unsigned long last_interrupt_time = 0;
  debounce(&istate.blue_btn, last_interrupt_time);
}

void green_btn_isr()
{
  static unsigned long last_interrupt_time = 0;
  debounce(&istate.green_btn, last_interrupt_time);
}

void reset_interrupt_state(int pin)
{
  switch (pin)
  {
  case BLUE_BTN:
    istate.blue_btn = 0;
    break;
  case GREEN_BTN:
    istate.green_btn = 0;
    break;
  }
}

volatile uint8_t get_interrupt_state(int pin)
{
  switch (pin)
  {
  case BLUE_BTN:
    return istate.blue_btn;
  case GREEN_BTN:
    return istate.green_btn;
  default:
    return 0;
  }
}

void setup_interrupts()
{
  reset_interrupt_state(BLUE_BTN);
  reset_interrupt_state(GREEN_BTN);

  // Turn pins into input pins with implicit pullup resistor
  pinMode(BLUE_BTN, INPUT_PULLUP);
  pinMode(GREEN_BTN, INPUT_PULLUP);

  // Attach ISR to each button
  attachInterrupt(digitalPinToInterrupt(BLUE_BTN), blue_btn_isr, RISING);
  attachInterrupt(digitalPinToInterrupt(GREEN_BTN), green_btn_isr, RISING);
}