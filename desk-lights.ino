#include <Arduino.h>
#include <FastLED.h>

#include "src/interrupt.h"
#include "src/state.h"
#include "src/leds.h"
#include "src/patterns.h"
#include "src/status.h"
#include "src/mem.h"

#define NUM_MODES 3

uint8_t mode = 0;

#define DESK_PIN 7
#define NUM_DESK_LEDS 177

#define STATUS_PIN 8
#define NUM_STATUS_LEDS 3

input_state_t state;
pattern_func_t patterns[NUM_MODES] = {manual_color, uniform_rainbow, rainbow_wave};

CRGB desk_lights[NUM_DESK_LEDS];
CRGB status_lights[NUM_STATUS_LEDS];

CRGB led_store[NUM_STATUS_LEDS];

void setup()
{
  init_state(&state);
  setup_interrupts();

  add_patterns(patterns, NUM_MODES);

  init_status_leds(status_lights, NUM_STATUS_LEDS);
  init_led_strip(desk_lights, NUM_DESK_LEDS);
}

void loop()
{
  update_state(&state);

  if (state.shift_btn)
  {
    if (get_interrupt_state(BLUE_BTN) == 1)
    {
      increment_selected();
      reset_interrupt_state(BLUE_BTN);
      reset_interrupt_state(GREEN_BTN);
    }
    else if (get_interrupt_state(GREEN_BTN) == 1)
    {
      int ndx = get_selected_ndx();
      store(led_store, NUM_STATUS_LEDS, desk_lights, NUM_DESK_LEDS, ndx);
      reset_interrupt_state(GREEN_BTN);
    }
  }
  else
  {
    if (get_interrupt_state(BLUE_BTN) == 1)
    {
      next_pattern();
      deactivate();
      reset_interrupt_state(BLUE_BTN);
      reset_interrupt_state(GREEN_BTN);
    }
    else if (get_interrupt_state(GREEN_BTN) == 1)
    {
      // recall save state
      int ndx = get_selected_ndx();
      load(led_store, NUM_STATUS_LEDS, desk_lights, NUM_DESK_LEDS, ndx);
      activate(ndx);
      set_pattern(pattern_noop);
      reset_interrupt_state(GREEN_BTN);
    }
    else
    {
      update_leds(&state);
    }
  }

  update_status_leds();

  FastLED.delay(1000 / 60);
}
